//nos traemos el módulo request json
const requestJson = require('request-json');
//URL base de conexión a mlab
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechutar9ed/collections/";
//APIKey de mlab. Viene en el archivo .env
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

//function getAccounts que devuelve cuentas de usuaro
function getAccountByUserIdV1(req, res) {
  console.log("GET /apitechu/v1/accounts/:userId");

  var userId = req.params.userId;
  console.log("Parámetro userId = " + userId);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("El cliente Mlab está creado");

  var query = 'q={"userId":' + userId + '}';
  console.log("query: " + query);

  httpClient.get("accounts?" + query + '&' + mLabAPIKey,
    function(err, resMLab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
         if (body.length > 0) {
           var response = body;
         } else {
           var response = {
             "msg" : "Cuenta de usuario " + userId + " no encontrada"
           }
           res.status(404);
         }
      }
      res.send(response);
    }
  )
}

module.exports.getAccountByUserIdV1 = getAccountByUserIdV1;
