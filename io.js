//función para escribir datos en fichero
const fs = require('fs');

function writeUserDataToFile(data) {
  console.log("writeUserDataToFile");

  var jsonuserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonuserData, "utf8",
    function(err) {
      if (err) {
         console.log(err);
      } else {
         console.log("Datos de usuarios guardados");
      }
    }
  )
}

module.exports.writeUserDataToFile = writeUserDataToFile;
