//nos traemos el módulo dotenv
require('dotenv').config();

//nos traemos el framework express
const express = require('express');

//creamos variable app
const app = express();

//nos traemos el módulo io
const io = require('./io');

//nos traemos el módulo UserController
const userController = require('./controllers/UserController');

//nos traemos el módulo AuthController
const authController = require('./controllers/AuthController');

//nos traemos el módulo AuthController
const accountController = require('./controllers/AccountController');

//para permitir invocar sin error por la URL por estar en otro dominio
var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

//para parsear por defecto a json
app.use(express.json());
//para permitir el CORS
app.use(enableCORS);

//asignamos puerto a la variable port cogiendo de variable de entorno
const port = process.env.PORT || 3000;

//pone la app a escuchar por el puerto asignado
app.listen(port);

//imprime por consola el puerto
console.log("API escuchando en el puerto BIP BIP " + port);

//define el método get que devuelve users con $top y $count
app.get('/apitechu/v1/users',userController.getUsersV1);

//define el método get que devuelve users con $top y $count. versión 2
app.get('/apitechu/v2/users',userController.getUsersV2);

//define el método get que devuelve un user a partir de su id
app.get('/apitechu/v2/users/:id',userController.getUserByIdV2);

//define el método post que da de alta un user
app.post('/apitechu/v1/users',userController.createUserV1);

//define el método post que da de alta un user. versión 2
app.post('/apitechu/v2/users',userController.createUserV2);

//define el método delete que da de baja un user
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);

//define el método post que hace login
app.post('/apitechu/v1/login',authController.loginV1);

//define el método post que hace logout
app.post('/apitechu/v1/logout/:id',authController.logoutV1);

//define el método post que hace login
app.post('/apitechu/v2/login',authController.loginV2);

//define el método post que hace logout
app.post('/apitechu/v2/logout/:id',authController.logoutV2);

//define el método get para consultar las cuentas de un cliente
app.get('/apitechu/v1/accounts/:userId',accountController.getAccountByUserIdV1);

app.get('/apitechu/v1/hello',
  function(req, res) {
    console.log("GET /apitechu/v1/hello");
//devuelve respuesta en formato JSON
    res.send({"msg" : "Hola desde API Tech_U"});
  }
);
