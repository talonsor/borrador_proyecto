//nos traemos el módulo io
const io = require('../io');
//nos traemos el módulo crypt
const crypt = require('../crypt');
//nos traemos el módulo request json
const requestJson = require('request-json');
//URL base de conexión a mlab
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechutar9ed/collections/";
//APIKey de mlab. Viene en el archivo .env
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

//function getUsers que devuelve users con $top y $count
function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
  users.slice(0, req.query.$top) : users;

  res.send(result);
}

//function getUsers que devuelve users
//versión 2 con mlab y mongodb
function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("El cliente está creado");

  httpClient.get("users?" + mLabAPIKey,
    function(err, resMLab, body) {

      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  )
}

//function getUserByIdV2 que devuelve un user a partir de su id
function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("El cliente está creado");

  var id = req.params.id;
  console.log("Parámetro id = " + id);

  var query = 'q={"id":' + id + '}';

  httpClient.get("users?" + query + '&' + mLabAPIKey,
    function(err, resMLab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo usuarios"
        }
        res.status(500);
      } else {
         if (body.length > 0) {
           var response = body[0];
         } else {
           var response = {
             "msg" : "Usuario no encontrado"
           }
           res.status(404);
         }
      }
      res.send(response);
    }
  )
}

//function createUser que da de alta un user
function createUserV1(req, res){
  console.log("POST /apitechu/v1/users");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name"  : req.body.last_name,
    "email"      : req.body.email
  }

  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDataToFile(users);
  console.log("Usuario añadido");
}

//function createUser que da de alta un user. versión 2
// con mlab y mongodb
function createUserV2(req, res){
  console.log("POST /apitechu/v2/users");

  console.log("id: " + req.body.id);
  console.log("first_name: " + req.body.first_name);
  console.log("last_name: " + req.body.last_name);
  console.log("email: " + req.body.email);
  console.log("password: " + req.body.password);

  var newUser = {
    "id"         : req.body.id,
    "first_name" : req.body.first_name,
    "last_name"  : req.body.last_name,
    "email"      : req.body.email,
    "password"   : crypt.hash(req.body.password)
  }

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("El cliente mLab está creado");

  httpClient.post("users?" + mLabAPIKey, newUser,
    function(err, resMLab, body) {
      console.log("Usuario insertado con éxito");
      res.status(201).send({"msg" : "Usuario insertado con éxito"});
    }
  )

}

//function deleteUser que borra un user
function deleteUserV1(req, res){
  console.log("DEL /apitechu/v1/users/:id");
  console.log("La id del user a borrar es " + req.params.id);
  var users = require('../usuarios.json');
  var deleted = false;
//
//delete recorriendo el array con for normal
//
  for (var i = 0; i < users.length; i++) {
    console.log("Bucle for -> i= " + i);
    if (users[i].id == req.params.id) {
       console.log("Borrar posición " + i + " del array");
       users.splice(i, 1);
       deleted = true;
       break;
    }
  }

//Escribe fichero una vez borrado usuario
  if (deleted) {
    io.writeUserDataToFile(users);
  }

  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg " : msg});
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
