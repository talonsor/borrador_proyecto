//nos traemos el módulo io
const io = require('../io');
//APIKey de mlab. Viene en el archivo .env
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
//nos traemos el módulo crypt
const crypt = require('../crypt');
//nos traemos el módulo request json
const requestJson = require('request-json');
//URL base de conexión a mlab
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechutar9ed/collections/";

//function login que hace login del usuario
function loginV1(req, res){
  console.log("POST /apitechu/v1/login");

  console.log("email: " + req.body.email);
  console.log("password: " + req.body.password);

  var users = require('../usuarios.json');
  var located = false;

  for (var i = 0; i < users.length; i++) {
    console.log("Búsqueda del usuario/password en users -> i = " + i);
    if ((users[i].email == req.body.email) &&
        (users[i].password == req.body.password)) {
       console.log("Coincide email/password en la posición i = " + i);
       located = true;
       break;
    }
  }

  var msg = located ?
  "Login correcto" : "Login incorrecto"
  console.log(msg);

  var respuesta = {};

  if (located) {
    users[i].logged = true;
    var loginId = users[i].id;
    io.writeUserDataToFile(users);
    respuesta.idUsuario = loginId;
    respuesta.mensaje =  msg;
    console.log("Email/Password correctos-> logged = true")
  } else {
    respuesta.mensaje =  msg;
  }
  res.send(respuesta);
}

//function que hace login del usuario. versión 2.
//con acceso a mLab
function loginV2(req, res){
  console.log("POST /apitechu/v2/login");

  console.log("email: " + req.body.email);
  console.log("password: " + req.body.password);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("El cliente mLab está creado");

// email y contraseña del body
  var email = req.body.email;
  var password = req.body.password;

// formateo de la query para recuperar el email de la BBDD
  var query = 'q={"email":' + '"' + email + '"' + '}';
  console.log("query: " + query);

// get para recuperar el email en BBDD
  httpClient.get("users?" + query + '&' + mLabAPIKey,
    function(errGET, resMLabGET, bodyGET) {

//Si hay error en el get devolvemos un 500
      if (errGET) {
        var response = {
          "msg" : "Error obteniendo usuarios"
        }
        res.status(500);
        res.send(response);
      } else {
//Si encuentra el email
         if (bodyGET.length > 0) {
            console.log("Antes de checkPassword");
            console.log("password en BBDD: " + bodyGET[0].password);
            console.log("password en entrada: " + password);
// Si la password es correcta
// Hacemos un put para incluir el campo logged=true
            console.log("crypt: " + crypt.checkPassword(password, bodyGET[0].password));
            if (crypt.checkPassword(password, bodyGET[0].password)){
                console.log("Password correcta");
            // variable para incluir el logged=true
                var putBody = '{"$set": {"logged":true}}';
// Put logged=true
                httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                  function(errPOST, resMLabPOST, bodyPOST){
                    console.log("errPOST: " + errPOST);
// Si hay error en el put devolvemos un status 500
                    if (errPOST) {
                       var response = {
                           "msg" : "Error al hacer login en BBDD"
                       }
                       res.status(500);
                       res.send(response);
                    } else {
// Si el login es correcto se devuelve OK y el id del usuario
                       var response = {};
                       response.msg = "Login correcto";
                       response.id  = bodyGET[0].id;
                       console.log("response: " + response.msg);
                       res.send(response);
                    }
                  }
                )
            } else {
   // Si la password no coincide devolvemos status 401
                     console.log("password incorrecta");
                     var response = {
                      "msg" : "Login incorrecto"
                     }
                     res.status(401);
                     res.send(response);
             }
         } else {
// Si no encuentra el email devolvemos status 401
                  console.log("email incorrecto");
                  var response = {
                   "msg" : "Login incorrecto"
                  }
                  res.status(401);
                  res.send(response);
          }
      }
    }
  )
}

//function logout que hace logout del usuario
function logoutV1(req, res){
  console.log("POST /apitechu/v1/logout/:id");
  console.log("User logout id = " + req.params.id);

  var users = require('../usuarios.json');
  var located = false;

  for (var i = 0; i < users.length; i++) {
    console.log("Búsqueda del idUsuario en users -> i = " + i);
    if ((users[i].id == req.params.id) &&
        (users[i].logged === true)) {
       console.log("Coincide idUsuario en la posición i = " + i);
       delete users[i].logged;
       io.writeUserDataToFile(users);
       console.log("users.logged borrado");
       located = true;
       break;
    }
  }

  var msg = located ?
  "Logout correcto" : "Logout incorrecto"

  console.log(msg);

  var respuesta = {};

  if (located) {
    respuesta.idUsuario = users[i].id;
    respuesta.mensaje =  msg;
  } else {
    respuesta.mensaje =  msg;
  }
  res.send(respuesta);

}

//function que hace logout del usuario. versión 2.
// acceso con mLab
function logoutV2(req, res){
  console.log("POST /apitechu/v2/logout/:id");
  console.log("User logout id = " + req.params.id);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("El cliente mLab está creado");

// variable para guardar el id
  var id = req.params.id;
  console.log("id: " + id);

// formateo de la query para recuperar el id de la BBDD
  var query = 'q={"id":' + id + '}';
  console.log("query: " + query);

  // get para recuperar el id en BBDD
  httpClient.get("users?" + query + '&' + mLabAPIKey,
    function(errGET, resMLabGET, bodyGET) {
      console.log("Dentro del get a mLab");
      console.log("errGET: " + errGET);
      //Si hay error en el get devolvemos un 500
            if (errGET) {
              var response = {
                "msg" : "Error obteniendo usuarios"
              }
              res.status(500);
              res.send(response);
            } else {
               console.log("bodyGET.length: " + bodyGET.length);
      //Si encuentra el id
               if (bodyGET.length > 0) {
                  console.log("id en BBDD: " + bodyGET[0].id);
                  // si el cliente está logado hacemos log out
                  if (bodyGET[0].logged == true){
                    console.log("Cliente con id: " + id + " logado");
                    // variable para quitar el logged=true (logout del user)
                    var putBody = '{"$unset":{"logged":""}}';
                    console.log("putBody: " + putBody);
                    // Put logged=false
                    httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                      function(errPOST, resMLabPOST, bodyPOST){
                            console.log("errPOST: " + errPOST);
                            console.log("logout correcto");
                            var response = {};
                            response.msg = "Logout correcto";
                            response.id = bodyGET[0].id;
                            res.send(response);
                          }
                        )
                  // si el usuario no está logado devolvemos error
                  } else {
                    console.log("Cliente con id: " + id + " no logado");
                    console.log("logout incorrecto");
                    var response = {
                     "msg" : "Logout incorrecto"
                    }
                    res.status(401);
                    res.send(response);
                  }
                // si el id del usuario no existe
                } else {
                  console.log("Cliente con id: " + id + " no existe");
                  console.log("logout incorrecto");
                  var response = {
                   "msg" : "Logout incorrecto"
                  }
                  res.status(401);
                  res.send(response);
                }
              }
      }
    )
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
